﻿init:
  # PEOPLES
    image mike        = Image("mike/mike1.png")
    image mike happy  = Image("mike/mike1.png")
    image mike yay    = Image("mike/mike1.png")

    image tom         = Image("mike/tom.jpg")

  # PLACES
    image lips1      = Image("bg/darkface.png")
    image flat1      = Image("bg/apartment-DOOR.png")
    image flat2      = Image("bg/apartment-LIGHT.png")
    image flat3      = Image("bg/apartment-HALL.png")
    image flat4      = Image("bg/apartment2.png")
    image flat5      = Image("bg/apartment1-close.png")
    image flat6      = Image("bg/apartment1-close2.png")
    image flat7      = Image("bg/apartment1-close3.png")

# name of the character.
    define m        = Character("Mike",color="#43A600")
    define t        = Character("Tom",color="#43A600")

# The game starts here.
label start:

    play music "music/home.mp3" fadein 1.0

    #play sound "audio/opening1.mp3"
    centered "I remember it well. The night that everything changed."
    #play sound "audio/opening2.mp3"
    centered "That one fucking evening when I looked out of a window and fate stabbed me with purpose."
    #play sound "audio/opening3.mp3"
    centered "All I had to do was, not look out of the window."
    #play sound "audio/opening4.mp3"
    centered "That was it. If the curtains were shut..."
    #play sound "audio/opening5.mp3"
    centered "I would still have a life"

    show lips1 with dissolve

    #play sound "audio/opening6.mp3"
    centered "Its not like it was a great life."
    #play sound "audio/opening7.mp3"
    centered "I had spent my 27 years doing"
    #play sound "audio/opening8.mp3"
    centered "well..."
    #play sound "audio/opening9.mp3"
    centered "Not very much."

    hide lips1 with dissolve

    centered "{b}7 Months earlier{/b}"

    scene flat1 with fade

    "I got home. It was eleven fifty eight."
    "I remember seeing the time on the microwave when I put the beers in the fridge"
    "I put beers in the fridge every night"
    "They were never there in the morning"
    "Shit, I'm rambling already. My name's Mike"

    show mike at left with dissolve

    m "I was a loser. I didn't know it then, but I was."
    m "I should have been enjoying my life. I was single, employed, care free"
    m "But instead I was lonely, angry and broke"
    m "..."
    m "I miss that now"
    m "..."
    m "What I should have done was..."
    m "sit myself down in that chair and ignore the flicker of light that caught my attention"

    scene flat4 with fade

    m "The curtains wasn't closed, there was a tiny flash of light from the street, so subtle it should have been missed"
    m "but I noticed it, and stepped forward to take a look"

    scene flat5 with fade







    # This ends the game.

    return
